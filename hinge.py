import matplotlib.pyplot as plt
from scipy.optimize import bisect
from steel_w import webF
from steel_f import flangeF
from concrete import slabF

bf = 5.5
tf = 0.345
tw = 0.25
h = 15.69+5.5
weff = 81.

def fxc(e_bottom, e_top):
  f = 0.

  def e(x):
    bary = x/h
    return (1-bary)*e_bottom + bary*e_top

  F = 0.
  t = 2.5/30.
  c = 15.69 + 3.0 + t/2.
  for i in range(30):
    F += slabF(e(c + i*t))
  f += t*weff*F

  return f

def fx(e_bottom, e_top):
  f = 0.

  def e(x):
    bary = x/h
    return (1-bary)*e_bottom + bary*e_top

  F = 0.
  t = tf/10.
  c = t/2.
  for i in range(10):
    F += flangeF(e(c + i*t))
  f += t*bf*F

  F = 0.
  t = (15.69 - tf - tf)/50.
  c = tf + t/2
  for i in range(50):
    F += webF(e(c + i*t))
  f += t*tw*F

  F = 0.
  t = tf/10.
  c = 15.69 - tf + t/2.
  for i in range(10):
    F += flangeF(e(c + i*t))
  f += t*bf*F

  F = 0.
  t = 2.5/30.
  c = 15.69 + 3.0 + t/2.
  for i in range(30):
    F += slabF(e(c + i*t))
  f += t*weff*F

  return f

def mz(e_bottom, e_top):
  m = 0.

  def e(x):
    bary = x/h
    return (1-bary)*e_bottom + bary*e_top

  M = 0.
  t = tf/10.
  c = t/2.
  for i in range(10):
    M += (c + i*t) * flangeF(e(c + i*t))
  m += t*bf*M

  M = 0.
  t = (15.69 - tf - tf)/50.
  c = tf + t/2.
  for i in range(50):
    M += (c + i*t) * webF(e(c + i*t))
  m += t*tw*M

  M = 0.
  t = tf/10.
  c = 15.69 - tf + t/2.
  for i in range(10):
    M += (c + i*t) * flangeF(e(c + i*t))
  m += t*bf*M

  M = 0.
  t = 2.5/30.
  c = 15.69 + 3.0 + t/2
  for i in range(30):
    M += (c + i*t) * slabF(e(c + i*t))
  m += t*weff*M

  return m

phi=[0.0]
theta=[0.0]
m=[0.0]
for i in range(500):
  e_bottom = (i+1) * 0.00001
  def partial(e_top):
    return fx(e_bottom, e_top)

  e_top = bisect(partial, -0.05, 0.0)
  na = h * e_bottom / (e_bottom - e_top)

  print("m = " + str(-mz(e_bottom, e_top)/12))
  print("e_b / na = " + str(e_bottom) + " / " + str(na) + " = " + str(e_bottom/na))
  print("theta = " + str(16.0 * e_bottom / na))
  print("fc = " + str(fxc(e_bottom, e_top)))
  phi.append(e_bottom / na)
  theta.append(16.0 * e_bottom / na)
  m.append(-mz(e_bottom, e_top) / 12)

plt.subplot(1, 2, 1).set_title("Moment Parametrized by Curvature")
plt.plot(phi, m, '.', markersize=1)
plt.xlabel("$\phi$ ($\mathrm{in}^{-1}$)")
plt.ylabel("$M$ ($\mathrm{k}\cdot\mathrm{ft}$)")
plt.grid()

plt.subplot(1, 2, 2).set_title("Moment Parametrized by 16\" Hinge Rotation")
plt.plot(theta, m, ".", markersize=1)
plt.xlabel("$\Theta$ (rad)")
plt.ylabel("$M$ ($\mathrm{k}\cdot\mathrm{ft}$)")
plt.grid()

plt.show()
