def slabF(e):
  if e > 0:
    return 0.0
  elif e > -0.0034:
    r = -e / 0.0021
    return -4 * (1.9 * r)/(1 + 0.9 * r**2.1)
  else:
    return 0.0

if __name__ == "__main__":
  import numpy as np
  import matplotlib.pyplot as plt

  X = np.linspace(-0.005, 0.005, 500)
  F = np.vectorize(slabF)(X)
  plt.plot(X, F)
  plt.xlabel("$\epsilon$ (in/in)")
  plt.ylabel("$\sigma$ (ksi)")
  plt.grid()
  plt.show()
