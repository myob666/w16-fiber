import math

conv = 0.1450

def model(e):
  e_pl = 0.0021
  e_y = 0.0042
  e_sh = 0.0042
  e_u = 0.1381
  e_f = 0.2082

  E = 203000*conv
  E_t = 16500*conv
  F_y = 444*conv
  F_u = 577*conv
  m = 1.0
  w = 0.6
  n = 0.1411

  if e < e_pl:
    return e*29000.0
  elif e < e_y:
    return (E-E_t)*e_pl + E_t*e
  else:
    e_green = math.log(1+e)
    e_ut = math.log(1+e_u)
    e_ft = math.log(1+e_f)

    if e_green < e_ut:
      return F_u*(e_green/e_ut)**n
    elif e_green < e_ft:
      return F_u
    else:
      return 0.0

def flangeF(e):
  sign = e / abs(e)
  return sign * model(abs(e))

if __name__ == "__main__":
  import numpy as np
  import matplotlib.pyplot as plt

  X = np.linspace(-0.25, 0.25, 50)
  F = np.vectorize(flangeF)(X)
  plt.plot(X, F)
  plt.xlabel("$\epsilon$ (in/in)")
  plt.ylabel("$\sigma$ (ksi)")
  plt.grid()
  plt.show()
