import math

conv = 0.1450

def model(e):
  e_pl = 0.0020
  e_y = 0.0020
  e_sh = 0.0148
  e_u = 0.1555
  e_f = 0.2138

  E = 202000*conv
  E_t = 0*conv
  F_y = 409*conv
  F_u = 573*conv
  m = 7.3
  w = 0.5
  n = 0.1611

  if e < e_pl:
    return e*29000.0
  elif e < e_y:
    return (E-E_t)*e_pl + E_t*e
  elif e < e_sh:
    return F_y
  else:
    e_green = math.log(1+e)
    e_ut = math.log(1+e_u)
    e_ft = math.log(1+e_f)

    if e_green < e_ut:
      return F_u*(e_green/e_ut)**n
    elif e_green < e_ft:
      return F_u
    else:
      return 0.0

def webF(e):
  sign = e / abs(e)
  return sign * model(abs(e))

if __name__ == "__main__":
  import numpy as np
  import matplotlib.pyplot as plt

  X = np.linspace(-0.25, 0.25, 50)
  F = np.vectorize(webF)(X)
  plt.plot(X, F)
  plt.xlabel("$\epsilon$ (in/in)")
  plt.ylabel("$\sigma$ (ksi)")
  plt.grid()
  plt.show()
